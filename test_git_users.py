import json
import unittest
from unittest import mock
import git_users as gu

RESPONSE = json.loads('{"id": 123, "login": "some_user1", "public_repos": 10, "followers": 20, "some_other_key": "some_value"}')


# Mocking requests.get
def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

        def raise_for_status(self):
            pass

    if args[0] == 'https://api.github.com/users/user1':
        return MockResponse(RESPONSE, 200)

    return MockResponse(None, 404)


class TestGetUserInfo(unittest.TestCase):

    @mock.patch.object(gu.requests.Session, 'get', side_effect=mocked_requests_get)
    def test_fetch(self, mock_get):
        json_data = gu.get_user_info('user1')
        self.assertIn(mock.call('https://api.github.com/users/user1', timeout=5), mock_get.call_args_list)
        # Test response
        self.assertEqual(json_data, RESPONSE)


class TestParseUserInfo(unittest.TestCase):
    def test_parse_user_info(self):
        self.assertEqual(gu.parse_user_info(RESPONSE), json.loads('{"user_id": 123, "login": "some_user1", "public_repos": 10, "followers": 20}'))


if __name__ == '__main__':
    unittest.main()
