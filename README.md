## Program
<br>
The purpose is to get a list of github users from a DynamoDB table, call an API to retrieve additional information like user ID, login, number of public repositories and followers. This information is then inserted into a target database.

## Limitations
This is a minimum viable product rather than a production ready solution. For example, the following functionality should be added/changed:
- Using HTTPAdapter to add retries with exponential back-off when calling APIs
- Implementing logging
- Multithreading and multiprocessing

## Testing
I used unittest which is built into the Python standard library. A simple unit test is test_parse_user_info which tests whether parse_user_info returns an expected result for a given input.
Function get_user_info cannot be tested directly because it would rely on getting a response from an external service. However, the GET request can be patched and the response object mocked. The test is checking whether the correct URL is called and if the response matches the expected response.
Additional tests should be added, e.g. checking whether:
- raise_for_status() method is called
- HTTPError is properly raised e.g. when internal server error occurs
- Testing return values for codes not equal to 200


Such tests should be done for other functions in the pipeline.

# Setup

1. Create the following DynamoDB tables:

- Table name: git-user
- Primary partition key: username (String)
- Primary sort key: None
<br>

- Table name: git-user-info
- Primary partition key: user_id (Number)
- Primary sort key: date (Number)

2. Create an IAM user and attach policies granting read and write access to git-user and git-user-info access

3. Configure aws-credentials with aws_access_key_id and aws_secret_access_key (see https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)