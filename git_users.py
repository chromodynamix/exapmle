from datetime import date
import requests
from requests.exceptions import HTTPError
from requests.sessions import HTTPAdapter
import boto3
from boto3.dynamodb.conditions import Key


BASE_URL = 'https://api.github.com'
SOURCE_TABLE = 'git-user'
TARGET_TABLE = 'git-user-info'
AWS_REGION_NAME = 'ap-southeast-2'


def parse_user_info(user_data):

    details = {
        "user_id": user_data['id'],
        "login": user_data['login'],
        "public_repos": user_data['public_repos'],
        "followers": user_data['followers']
        }

    return details


def dynamodb_connect(table):
    try:
        client = boto3.resource('dynamodb', AWS_REGION_NAME)
        tbl = client.Table(table)
    except Exception:
        print("And error has occured connecting to DynamoDB.")
        # Raise exception to preserve stack trace
        raise
    return tbl


def get_users():
    table = dynamodb_connect(SOURCE_TABLE)
    filter_expression = Key('is_active').eq('yes')
    response = table.scan(FilterExpression=filter_expression)

    active_user_list = []
    for i in response['Items']:
        active_user_list.append(i['username'])

    while 'LastEvaluatedKey' in response:
        response = table.scan(
            FilterExpression=filter_expression,
            ExclusiveStartKey=response['LastEvaluatedKey']
            )
        for i in response['Items']:
            active_user_list.append(i['username'])

    return active_user_list


def insert_user_info(user_info):
    timestamp = int(date.today().strftime('%Y%m%d'))
    user_info_date = [{'date': timestamp, **item} for item in user_info]

    table = dynamodb_connect(TARGET_TABLE)
    try:
        with table.batch_writer() as batch:
            for item in user_info_date:
                batch.put_item(Item=item)
        print(f"User information successfully written into '{TARGET_TABLE}' table.")
    except Exception as err:
        print(f'ERROR: {err}')
        # Raise exception to preserve stack trace
        raise


def get_user_info(username):
    url = f'{BASE_URL}/users/{username}'
    session = requests.Session()
    session.mount(url, HTTPAdapter(max_retries=(5)))
    try:
        response = session.get(url, timeout=5)
        # response = requests.get(url, timeout=5)
        # Returns an HTTPError object if an error has occurred
        response.raise_for_status()
    except HTTPError as http_err:
        print(f'HTTP error: {http_err}')
    except Exception as err:
        print(f'ERROR: {err}')

    if response.status_code == 200:
        return response.json()
    else:
        return None


def main():
    all_users = get_users()
    user_info_list = []
    if all_users:
        for user in all_users:
            print(f"Fetching details for {user}.")
            user_data = get_user_info(user)
            if user_data:
                parsed_user_data = parse_user_info(user_data)
                user_info_list.append(parsed_user_data)
            else:
                print(f"No data retrieved for user {user}.")
        if user_info_list:
            insert_user_info(user_info_list)
        else:
            print(f"Nothing to write to '{TARGET_TABLE}' table.")
    else:
        print(f"No users retrieved from '{SOURCE_TABLE}' table.")


if __name__ == "__main__":
    main()
